const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  // 用户
  token: state => state.manager.token,
  userInfo: state => state.manager.userInfo,
  // 权限路由
  routes: state => state.permission.routes,
  originRoutes: state => state.permission.originRoutes,
  // 标签导航
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  // 主题
  theme: state => state.settings.theme,
  // 图片库
  photoGroups: state => state.photoGroup.photoGroups,
  // 图片库选择的图片数组
  photoChooseList: state => state.photoGroupImage.photoChooseList,
  photoList: state => state.photoGroupImage.photoList,
  photoTotal: state => state.photoGroupImage.photoTotal,
  // 上传的urls数组
  uploadUrls: state => state.upload.uploadUrls
}
export default getters
