import { photoList } from '@/api/photo'

const getDefaultState = () => {
  return {
    photoChooseList: [],
    photoList: [],
    photoTotal: 0,
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_CHOOSE_LIST: (state, list) => {
    state.photoChooseList = list
  },
  PUSH_CHOOSE_ITEM: (state, item) => {
    state.photoChooseList.push(item)
  },
  REMOVE_CHOOSE_BY_INDEX: (state, index) => {
    state.photoChooseList.splice(index, 1)
  },
  SET_PHOTO_LIST: (state, list) => {
    state.photoList = list
  },
  PUSH_PHOTO_LIST: (state, list) => {
    state.photoList = list.concat(state.photoList)
  },
  PUSH_PHOTO_TOTAL: (state, total) => {
    state.photoTotal += total
  },
  SET_PHOTO_TOTAL: (state, total) => {
    state.photoTotal = total
  }
}

const actions = {
  getPhotoList({ commit }, data) {
    return new Promise((resolve, reject) => {
      photoList(data).then(response => {
        const { page } = response
        commit('SET_PHOTO_LIST', page.data)
        commit('SET_PHOTO_TOTAL', page.total)
        resolve(page.data)
      }).catch(error => {
        reject(error)
      })
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

