import variables from '@/styles/element-variables.scss'
import defaultSettings from '@/settings'
import { getSettings, setSettings } from '@/utils/settingStorage'

const { showSettings } = defaultSettings
const settings = getSettings()

const state = {
  // 主题
  theme: settings.theme || variables.theme,
  // 是否显示右侧设置按钮
  showSettings: showSettings,
  // 是否使用滚动固定头部
  fixedHeader: Boolean(settings.fixedHeader),
  // 是否显示侧边栏的logo
  sidebarLogo: Boolean(settings.sidebarLogo),
  // 是否使用标签导航
  tagsView: Boolean(settings.tagsView),
  // 是否只保持一个子菜单的展开
  menuUniOpen: Boolean(settings.menuUniOpen),
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
    setSettings(data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

