
const getDefaultState = () => {
  return {
    uploadUrls: []
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_LIST: (state, list) => {
    state.uploadUrls = list
  },
}

export default {
  namespaced: true,
  state,
  mutations,
}

