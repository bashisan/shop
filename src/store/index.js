import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import manager from './modules/manager'
import permission from './modules/permission'
import tagsView from './modules/tagsView'
import photoGroup from './modules/photoGroup'
import photoGroupImage from './modules/photoGroupImage'
import upload from './modules/upload'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    manager,
    permission,
    tagsView,
    photoGroup,
    photoGroupImage,
    upload
  },
  getters
})

export default store
