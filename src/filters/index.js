import { formatTime as formatTime2, isSuper, int2Bool } from '@/utils/util'

/**
 * 自定义时间格式化
 * @param any time 
 * @param string reg 
 */
export const formatTime = formatTime2

/**
 * 是否超管标签
 * @param {*} val 
 * @param {*} type 
 * @returns 
 */
export const superTag = (val, type = 1) => {
  if (isSuper(val)) {
    return type == 1 ? '是' : 'success'
  } else {
    return type == 1 ? '否' : 'danger'
  }
}

/**
 * 正数转标签 1=是 0=否
 * @param {*} val 
 * @param {*} type 
 * @returns 
 */
export const int2BoolTag = (val, type = 1) => {
  if (int2Bool(val)) {
    return type == 1 ? '是' : 'success'
  } else {
    return type == 1 ? '否' : 'danger'
  }
}

/**
 * @param {*} val 
 * @param {*} type 
 * @returns 
 */
export const statusTag = (val, type = 1) => {
  if (int2Bool(val)) {
    return type == 1 ? '下线' : 'danger'
  } else {
    return type == 1 ? '上线' : 'success'
  }
}
