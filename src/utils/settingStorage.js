
/**
 * 设置相关
 */
const settingsKey = 'shopSettings'
const local = window.localStorage;

export function getSettings() {
  const data = local.getItem(settingsKey) || {}
  try {
    return JSON.parse(data)
  } catch (error) {
    return data
  }
}

export function setSettings({ key, value }) {
  const settings = getSettings()
  settings[key] = value

  return local.setItem(settingsKey, JSON.stringify(settings));
}

export function removeSettings() {
  return local.removeItem(settingsKey)
}
