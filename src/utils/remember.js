// 账号键
const AccountKey = 'shopacc'
const Local = window.localStorage;

// 获取 账号密码
export function getAccPass() {
  return JSON.parse(Local.getItem(AccountKey));
}

// 设置 账号密码
export function setAccPass(ap) {
  return Local.setItem(AccountKey, JSON.stringify(ap));
}

// 清除 账号密码
export function removeAccPass() {
  return Local.removeItem(AccountKey);
}
