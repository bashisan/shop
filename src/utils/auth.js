const TokenKey = 'shoptoken'
const Session = window.sessionStorage;

export function getToken() {
  return Session.getItem(TokenKey)
}

export function setToken(token) {
  return Session.setItem(TokenKey, token);
}

export function removeToken() {
  return Session.removeItem(TokenKey)
}
