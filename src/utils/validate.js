
/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * 检测是否是有效的网络图片
 * @param {*} url 
 * @returns 
 */
export const isNetImage = (url) => {
  return new Promise((resolve, reject) => {
    if (
      !url || 
      !/^(https?)/.test(url) || 
      !/(gif|jpg|jpeg|png|webp|GIF|JPG|PNG|WEBP)/.test(url)
    ) {
      reject(new Error(`该图片地址不合法 ${url}`))
    }

    const imgInstance = new Image()
    imgInstance.src = url
    imgInstance.onload = resolve
    imgInstance.onerror = () => reject(new Error(`该图片加载失败 ${url}`))
  })
}

/**
 * 验证有效手机号
 * @param {*} phone 
 * @returns 
 */
export const isPhone = phone => {
	if (!phone) return false
	phone = ('' + phone).replace(/\s+/g, '')
	if (phone.length == 11 && /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1})|)+\d{8})$/.test(phone)) {
		return true
	} else {
		return false
	}
}

/**
 * 文件类型验证
 * @param {*} file 文件
 * @param {*} fileMainType 类型大类
 * @param {*} fileTypes 类型数组 如['jpg','png']
 * @returns true 合法 false 不合法
 */
export const checkFileType = (file, fileMainType = 'image', fileTypes) => {
  const currentFileType = file.type.split('/')
  return currentFileType[0] === fileMainType && fileTypes.includes(currentFileType[1])
}

/**
 * 文件大小验证
 * @param {*} file 文件
 * @param {*} fileSize 合法大小 1表示1M
 * @returns true 合法 false 不合法
 */
export const checkFileSize = (file, fileSize = 1) => {
  return file.size / 1024 / 1024 <= fileSize
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

// 验证字符串是否为空
export function isStrEmpty(str) {
  if (typeof str == 'undefined' || !str || !str.replace(/\s+/g, '')) {
    return true;
  } else {
    return false;
  }
}

export const validateUsername = (rule, value, callback) => {
  if (isStrEmpty(value)) {
    callback(new Error("请输入用户名"));
  } else if (value.length < 5) {
    callback(new Error("用户名至少5位"));
  } else {
    callback();
  }
};

export const validatePassword = (rule, value, callback) => {
  if (isStrEmpty(value)) {
    callback(new Error("请输入密码"));
  } else if (value.length < 6 || value.length > 18) {
    callback(new Error("请输入6-18位的密码"));
  } else {
    callback();
  }
};

export const validatePhone = (rule, value, callback) => {
  if (!isPhone(value)) {
    callback(new Error("手机号格式不正确"));
  } else {
    callback();
  }
};

export const validateExternal = (rule, value, callback) => {
  if (!isExternal(value)) {
    callback(new Error("URL地址格式不正确"));
  } else {
    callback();
  }
};