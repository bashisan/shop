module.exports = {

  title: '个人商城',

  /**
   * api域名
   */
  apiHost: process.env.VUE_APP_BASE_API,

  /**
   * api地址前缀
   */
  apiPrefix: 'api',

  /**
   * api版本
   */
  apiVersion: 'v1',

  /**
   * 反向代理服务器地址
   */
  proxyUrl: '/shop',
  
  /**
   * @type {boolean} true | false
   * @description 是否显示右侧设置按钮
   */
  showSettings: true,
}
