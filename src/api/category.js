import request from '@/utils/request'
import baseURL from './baseUrl'

export function categoryList() {
  return request({
    url: `${baseURL}/category/list`,
    method: 'GET'
  })
}

export function ChangeCateStatus(id) {
  return request({
    url: `${baseURL}/category/changeStatus/${id}`,
    method: 'GET'
  })
}