import settings from '@/settings'

const baseURL = `${settings.proxyUrl}/${settings.apiPrefix}/${settings.apiVersion}`

export default baseURL