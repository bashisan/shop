import request from '@/utils/request'
import baseURL from './baseUrl'

export function authList() {
  return request({
    url: `${baseURL}/auth/list`,
    method: 'GET'
  })
}

export function authInfo(id) {
  return request({
    url: `${baseURL}/auth/info/${id}`,
    method: 'GET'
  })
}

export function authCreate(data) {
  return request({
    url: `${baseURL}/auth/create`,
    method: 'POST',
    data
  })
}

export function authUpdate(data) {
  return request({
    url: `${baseURL}/auth/update`,
    method: 'POST',
    data
  })
}

export function authChangeStatus(id) {
  return request({
    url: `${baseURL}/auth/changeStatus/${id}`,
    method: 'GET'
  })
}