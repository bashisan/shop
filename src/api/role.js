import request from '@/utils/request'
import baseURL from './baseUrl'

export function roleList() {
  return request({
    url: `${baseURL}/role/list`,
    method: 'GET'
  })
}

export function roleInfo(id) {
  return request({
    url: `${baseURL}/role/info/${id}`,
    method: 'GET'
  })
}

export function roleCreate(data) {
  return request({
    url: `${baseURL}/role/create`,
    method: 'POST',
    data
  })
}

export function roleUpdate(data) {
  return request({
    url: `${baseURL}/role/update`,
    method: 'POST',
    data
  })
}

export function roleDelete(id) {
  return request({
    url: `${baseURL}/role/delete/${id}`,
    method: 'GET'
  })
}