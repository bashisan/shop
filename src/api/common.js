import request from '@/utils/request'
import baseURL from './baseUrl'

/**
 * 上传
 * @param {*} file 文件对象
 * @param {*} dir 上传路径
 * @returns 
 */
export function upload(file, dir) {
  const formData = new FormData()
  formData.append('file', file)
  formData.append('dir', dir)

  return request({
    url: `${baseURL}/common/upload/single`,
    method: 'POST',
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

/**
 * 上传
 * @param {*} files 文件对象数组
 * @param {*} dir 上传路径
 * @returns 
 */
export function uploadMultiple(files, dir) {
  const formData = new FormData()
  files.forEach(file => {
    formData.append('files[]', file)
  });
  formData.append('dir', dir)

  return request({
    url: `${baseURL}/common/upload/multiple`,
    method: 'POST',
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}