import request from '@/utils/request'
import baseURL from './baseUrl'

export function login(data) {
  return request({
    url: `${baseURL}/manager/login`,
    method: 'POST',
    data
  })
}

export function logout() {
  return request({
    url: `${baseURL}/manager/logout`,
    method: 'GET'
  })
}

export function mineInfo() {
  return request({
    url: `${baseURL}/manager/mineInfo`,
    method: 'GET'
  })
}

export function managerList(data) {
  return request({
    url: `${baseURL}/manager/list`,
    method: 'POST',
    data
  })
}

export function managerInfo(id) {
  return request({
    url: `${baseURL}/manager/info/${id}`,
    method: 'GET'
  })
}

export function managerCreate(data) {
  return request({
    url: `${baseURL}/manager/create`,
    method: 'POST',
    data
  })
}

export function managerUpdate(data) {
  return request({
    url: `${baseURL}/manager/update`,
    method: 'POST',
    data
  })
}

// 冻结/恢复
export function managerChangeStatus(data) {
  return request({
    url: `${baseURL}/manager/changeStatus`,
    method: 'POST',
    data
  })
}

// 设置角色
export function managerSetRole(data) {
  return request({
    url: `${baseURL}/manager/setRole`,
    method: 'POST',
    data
  })
}

export function leftMenu() {
  return request({
    url: `${baseURL}/manager/authMenu`,
    method: 'GET'
  })
}