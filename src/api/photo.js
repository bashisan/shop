import request from '@/utils/request'
import baseURL from './baseUrl'

export function photoList(data) {
  return request({
    url: `${baseURL}/image/list`,
    method: 'POST',
    data
  })
}

export function photoCreate(data) {
  return request({
    url: `${baseURL}/image/create`,
    method: 'POST',
    data
  })
}