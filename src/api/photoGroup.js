import request from '@/utils/request'
import baseURL from './baseUrl'

export function photoGroupList() {
  return request({
    url: `${baseURL}/photoGroup/list`,
    method: 'GET'
  })
}

export function photoGroupCreate(data) {
  return request({
    url: `${baseURL}/photoGroup/create`,
    method: 'POST',
    data
  })
}

export function photoGroupUpdate(data) {
  return request({
    url: `${baseURL}/photoGroup/update`,
    method: 'POST',
    data
  })
}

export function photoGroupDelete(id) {
  return request({
    url: `${baseURL}/photoGroup/delete/${id}`,
    method: 'GET'
  })
}